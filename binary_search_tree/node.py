"""
Programming for linguists

Implementation of the Node abstraction for data structure Binary Tree
"""


class Node:
    """
    The Node abstraction
    """
    def __init__(self, value, root=None, left_node=None, right_node=None):
        pass

    def __lt__(self, other) -> bool:
        """
        Overload of the < operator
        :param other: an instance of Node to compare with
        :return: True if self < value
                 False otherwise
        """

    def __gt__(self, other) -> bool:
        """
        Overload of the > operator
        :param other: an instance of Node to compare with
        :return: True if self > value
                 False otherwise
        """
