"""
Programming for linguists

Classes and functions for Operators
"""

from typing import Optional


# pylint: disable=invalid-name
class Operator_:
    priority = None

    @staticmethod
    def function(first: float, second: float) -> float:
        pass


class Plus(Operator_):
    pass


class Minus(Operator_):
    pass


class Multiplier(Operator_):
    pass


class Divider(Operator_):
    pass


class Power(Operator_):
    pass


class OperatorCreator:
    @staticmethod
    def create_operator(operator: str) -> Optional[Operator_]:
        pass


# pylint: disable=unused-argument
def get_operator_priority(operator: str) -> int:
    pass
