"""
Programming for linguists

Class for Reverse Polish Notation
"""

from typing import Optional


class ReversePolishNotation:
    """
    Reverse Polish Notation class
    """

    def __init__(self):
        pass

    def get_next_element(self) -> Optional[str]:
        """
        Get next element in Reverse Polish Notation
        :return: next element from rpn if exists or None
        """

    def put_number(self, number: str):
        """
        Put number to the RPN - getting an argument number as string type and save as float
        :param number: digit in string type
        """

    def put_operator(self, operator: str):
        """
        Put operator to the RPN - getting an argument operator as string type and save as Operator_
        :param operator: operator in string type
        """
